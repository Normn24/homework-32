import { useState, useEffect } from "react";
import {
  BrowserRouter as Router,
  NavLink,
  Routes,
  Route,
} from "react-router-dom";
import ProductList from "./components/ProductsList/ProductsList";
import Header from "./components/Header/Header";
import Favourites from "./components/Favourites/Favourites";
import Basket from "./components/Basket/Basket";
import "./AppRouters.scss";

export default function AppRouters() {
  const [products, setProducts] = useState([]);

  useEffect(() => {
    async function fetchData() {
      await fetch("../products.json")
        .then((response) => response.json())
        .then((data) => setProducts(data));
    }

    fetchData();
  }, []);

  const [cartItems, setCartItems] = useState([]);
  const [favorites, setFavorites] = useState([]);

  useEffect(() => {
    const storedCartItems = JSON.parse(localStorage.getItem("cartItems")) || [];
    const storedFavorites = JSON.parse(localStorage.getItem("favorites")) || [];
    setCartItems(storedCartItems);
    setFavorites(storedFavorites);
  }, []);

  const addToCart = (product) => {
    const isOnBasket = cartItems.some(
      (item) => item.article === product.article
    );

    if (!isOnBasket) {
      const updatedCartItems = [...cartItems, product];
      setCartItems(updatedCartItems);
      localStorage.setItem("cartItems", JSON.stringify(updatedCartItems));
      return 1;
    }
    return 0;
  };

  const addToFavorites = (product) => {
    const updatedFavorites = [...favorites, product];
    setFavorites(updatedFavorites);
    localStorage.setItem("favorites", JSON.stringify(updatedFavorites));
  };

  const removeFromFavorites = (product) => {
    const updatedFavorites = favorites.filter(
      (favProduct) => favProduct.article !== product.article
    );
    setFavorites(updatedFavorites);
    localStorage.setItem("favorites", JSON.stringify(updatedFavorites));
  };

  const removeFromCart = (productToRemove) => {
    const updatedCart = cartItems.filter(
      (product) => product.article !== productToRemove.article
    );
    setCartItems(updatedCart);
    localStorage.setItem("cartItems", JSON.stringify(updatedCart));
  };

  return (
    <Router>
      <header className="header">
        <div className="header__links">
          <NavLink to="./">Home</NavLink>
          <NavLink to="./basket" style={{ marginLeft: "10px" }}>
            Basket
          </NavLink>
          <NavLink to="./favourites" style={{ marginLeft: "10px" }}>
            Favourites
          </NavLink>
        </div>

        <div className="header__icon">
          <NavLink to="./basket">
            <Header cartItemsCount={cartItems.length} />
          </NavLink>
          <NavLink to="./favourites">
            <Header favoritesCount={favorites.length} />
          </NavLink>
        </div>
      </header>
      <Routes>
        <Route
          path="/"
          element={
            <ProductList
              products={products}
              onAddToCart={addToCart}
              onAddToFavorites={addToFavorites}
              onRemoveFromFavorites={removeFromFavorites}
            />
          }
        />
        <Route
          path="/favourites"
          element={
            <Favourites
              products={favorites}
              onAddToFavorites={addToFavorites}
              onRemoveFromFavorites={removeFromFavorites}
            />
          }
        />
        <Route
          path="/basket"
          element={
            <Basket products={cartItems} removeFromCart={removeFromCart} />
          }
        />
      </Routes>
      <div className="app"></div>
    </Router>
  );
}
