import PropTypes from "prop-types";
import "./Header.scss";

export default function Header({ cartItemsCount, favoritesCount }) {
  return (
    <>
      {cartItemsCount && (
        <span>
          <i className="fa fa-shopping-cart"></i>
          {cartItemsCount}
        </span>
      )}
      {favoritesCount && (
        <span>
          <i className="fa fa-heart"></i>
          {favoritesCount}
        </span>
      )}
    </>
  );
}

Header.propTypes = {
  cartItemsCount: PropTypes.number,
  favoritesCount: PropTypes.number,
};
