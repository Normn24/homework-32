import { useState, useEffect } from "react";
import PropTypes from "prop-types";
import Modal from "../Modal/Modal";
import "./ProductCard.scss";

export default function ProductCard({
  product,
  onAddToCart,
  onAddToFavorites,
  onRemoveFromFavorites,
}) {
  const [isModalOpen, setIsModalOpen] = useState(false);
  const [isFavorited, setIsFavorited] = useState(false);
  const [isCartItems, setCartItems] = useState(false);
  const { name, price, image, article, genre } = product;

  const openModal = () => setIsModalOpen(true);
  const closeModal = () => setIsModalOpen(false);

  const handleAddToCart = () => {
    openModal();
  };

  useEffect(() => {
    const favorites = JSON.parse(localStorage.getItem("favorites")) || [];
    const cartItems = JSON.parse(localStorage.getItem("cartItems")) || [];
    setIsFavorited(
      favorites.some((favProduct) => favProduct.article === article)
    );
    setCartItems(
      cartItems.some((cardProduct) => cardProduct.article === article)
    );
  }, [article]);

  const handleAddToFavorites = () => {
    const favorites = JSON.parse(localStorage.getItem("favorites")) || [];

    if (isFavorited) {
      const updatedFavorites = favorites.filter(
        (favProduct) => favProduct.article !== article
      );
      localStorage.setItem("favorites", JSON.stringify(updatedFavorites));
      onRemoveFromFavorites(product);
    } else {
      localStorage.setItem(
        "favorites",
        JSON.stringify([...favorites, product])
      );
      onAddToFavorites(product);
    }
    setIsFavorited(!isFavorited);
  };

  return (
    <div className="product__card">
      <img src={image} alt={name} />
      <div className="product__body">
        <h3>{name}</h3>
        <p>Article: {article}</p>
        <p>Genre: {genre}</p>
        <div className="button__container">
          <p>{price}$</p>
          <button className="btn__busket" onClick={handleAddToCart}>
            Add to busket
          </button>
          <button
            className={`btn__favourite ${
              isFavorited ? "active__favourite" : ""
            }`}
            onClick={handleAddToFavorites}
          >
            &#9733;
          </button>
        </div>
      </div>
      {isCartItems ? (
        <Modal
          isOpen={isModalOpen}
          onClose={closeModal}
          message={`"${name}" is already in the basket`}
        />
      ) : (
        <Modal
          isOpen={isModalOpen}
          onClose={closeModal}
          message={`Add "${name}" to the basket?`}
          onConfirm={() => {
            onAddToCart(product);
            closeModal();
            setCartItems(!isCartItems);
          }}
        />
      )}
    </div>
  );
}

ProductCard.propTypes = {
  product: PropTypes.shape({
    name: PropTypes.string.isRequired,
    price: PropTypes.number.isRequired,
    image: PropTypes.string.isRequired,
    article: PropTypes.number.isRequired,
    genre: PropTypes.string.isRequired,
  }).isRequired,
  onAddToCart: PropTypes.func.isRequired,
  onAddToFavorites: PropTypes.func.isRequired,
  onRemoveFromFavorites: PropTypes.func.isRequired,
};
