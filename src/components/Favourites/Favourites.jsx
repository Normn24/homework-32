import PropTypes from "prop-types";
import FavouriteCard from "./FavouriteCard";
import "./Favourites.scss";

export default function Favourites({ products, onRemoveFromFavorites }) {
  return (
    <div className="favourite__container">
      <h1>Favourite Page</h1>
      <div className="favourite__cards">
        {products.length == 0 ? (
          <div>
            <h2>Your favourite page is empty</h2>
          </div>
        ) : (
          <>
            {products.map((product, id) => (
              <FavouriteCard
                key={id}
                product={product}
                onRemoveFromFavorites={onRemoveFromFavorites}
              />
            ))}
          </>
        )}
      </div>
    </div>
  );
}

Favourites.propTypes = {
  products: PropTypes.arrayOf(PropTypes.object).isRequired,
  onRemoveFromFavorites: PropTypes.func.isRequired,
};
