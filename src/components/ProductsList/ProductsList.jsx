import ProductCard from "../ProductCard/ProductCard";
import PropTypes from "prop-types";
import "./ProductList.scss";
export default function ProductList({
  products,
  onAddToCart,
  onAddToFavorites,
  onRemoveFromFavorites,
}) {
  return (
    <div className="product__list">
      {products.map((product, index) => (
        <ProductCard
          key={index}
          product={product}
          onAddToCart={onAddToCart}
          onAddToFavorites={onAddToFavorites}
          onRemoveFromFavorites={onRemoveFromFavorites}
        />
      ))}
    </div>
  );
}

ProductList.propTypes = {
  products: PropTypes.arrayOf(PropTypes.object).isRequired,
  onAddToCart: PropTypes.func.isRequired,
  onAddToFavorites: PropTypes.func.isRequired,
  onRemoveFromFavorites: PropTypes.func.isRequired,
};
