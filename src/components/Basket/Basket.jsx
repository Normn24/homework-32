import PropTypes from "prop-types";
import BasketCard from "./BasketCard";
import "./Basket.scss";

export default function Basket({ products, removeFromCart }) {
  return (
    <div className="basket__container">
      <h1>Basket</h1>
      {products.length === 0 ? (
        <div>
          <h2>You have not added anything to your basket yet</h2>
        </div>
      ) : (
        <>
          {products.map((product) => (
            <BasketCard
              key={product.article}
              product={product}
              removeFromCart={removeFromCart}
            />
          ))}
        </>
      )}
    </div>
  );
}

Basket.propTypes = {
  products: PropTypes.arrayOf(PropTypes.object).isRequired,
  removeFromCart: PropTypes.func.isRequired,
};
