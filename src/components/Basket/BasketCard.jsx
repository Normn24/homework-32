import { useState } from "react";
import Modal from "../Modal/Modal";
import PropTypes from "prop-types";

export default function BasketCard({ product, removeFromCart }) {
  const { name, price, image, article, genre } = product;
  const [isModalOpen, setIsModalOpen] = useState(false);

  const openModal = () => setIsModalOpen(true);
  const closeModal = () => setIsModalOpen(false);

  const handleDeleteToCart = () => {
    openModal();
  };

  return (
    <div className="basket__card">
      <img src={image} alt={name} />
      <div className="basket__body">
        <div>
          <h3>{name}</h3>
          <p>Article: {article}</p>
          <p>Genre: {genre}</p>
        </div>
        <div className="btnBasket__container">
          <p>{price}$</p>
          <button className="btn__busket" onClick={handleDeleteToCart}>
            &times;
          </button>
        </div>
      </div>

      <Modal
        isOpen={isModalOpen}
        onClose={closeModal}
        message={`Delete "${name}" to the basket?`}
        onConfirm={() => {
          removeFromCart(product);
          closeModal();
        }}
      />
    </div>
  );
}

BasketCard.propTypes = {
  product: PropTypes.shape({
    name: PropTypes.string.isRequired,
    price: PropTypes.number.isRequired,
    image: PropTypes.string.isRequired,
    article: PropTypes.number.isRequired,
    genre: PropTypes.string.isRequired,
  }).isRequired,
  removeFromCart: PropTypes.func.isRequired,
};
